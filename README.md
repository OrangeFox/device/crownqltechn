# Device Tree for Samsung Galaxy Note 9 (SM-N9600)
Device Tree Made by Klabit and edited by Renatoh Ribeiro
## Specs

|        Component        |          Specification            |
| :---------------------- | :-------------------------------- |
| Chipset                 | Qualcomm SDM845 Snapdragon 845    |
| Memory                  | 6 GB or 8GB                       |
| Storage                 | 128GB or 512GB                    |
| Battery                 | 4000 mAh (non-removable)          |
| Dimensions              | 147.7 x 68.7 x 8.5 m              |
| Display                 | 1440 x 2960 pixels, 18.5:9, 516PPI|
| Release Date            | 2018 March                        |

## Device Picture

![Galaxy Note 9](https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-note9-r1.jpg "Galaxy Note 9")
